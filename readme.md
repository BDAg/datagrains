# Apresentação
https://www.youtube.com/watch?v=rQIxN8VUen8

# Instalação

```
# clone the repo
$ git clone https://gitlab.com/BDAg/datagrains.git

# change the working directory to sherlock
$ cd datagrains

# install the requirements
$ python3 -m pip install -r requirements.txt
```

# Uso
```
$ python .\Datagrains.py --help
usage: Datagrains.py [-h] -p PRACAS

optional arguments:
  -h, --help            show this help message and exit        
  -p PRACAS, --pracas PRACAS
                        Escolha as praças ex: milho, cafe, soja
```